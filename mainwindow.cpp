#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "literalmodel.h"

#include <QColor>
#include <QPointF>
#include <QPaintDevice>
#include <QVariantList>
#include <QFileDialog>
#include <QDir>
#include <QTextStream>
#include <QFileInfo>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	hasChanged = false;

	highlight = new QCheckBox("Highlight");
	highlight->setChecked(false);
	statusBar()->addPermanentWidget(highlight);

	painter = new QPainter;
	command = Command::getInstance();
	command->setPainter(painter);

	ui->L->setModel(new LiteralModel(command, this));

	connect(command, SIGNAL(imageChanged()), this, SLOT(parseAndPaint()));
	connect(command, SIGNAL(changeSceneRect(double,double)), ui->D, SLOT(sceneRect(double,double)));

	connect(highlight, SIGNAL(toggled(bool)), this, SLOT(switchHighlight(bool)));

	connect(ui->L, SIGNAL(textChanged()), this, SLOT(parseAndPaint()));
	connect(ui->D, SIGNAL(somethingChange()), this, SLOT(parseAndPaint()));
	connect(ui->D, SIGNAL(posClick(double,double)), ui->L, SLOT(insertPoint(double,double)));

	connect(ui->actionNew_Text, SIGNAL(triggered()), this, SLOT(newText()));
	connect(ui->actionLoad_Text, SIGNAL(triggered()), this, SLOT(loadText()));
	connect(ui->actionSave_Text, SIGNAL(triggered()), this, SLOT(saveTxt()));
    connect(ui->actionSave_Text_As, SIGNAL(triggered()), this, SLOT(saveTxtAs()));
    connect(ui->actionSave_SVG, SIGNAL(triggered()), this, SLOT(saveSVG()));
    connect(ui->actionSave_Image, SIGNAL(triggered()), this, SLOT(saveImage()));

	connect(command, SIGNAL(namesChanged()), ui->L, SLOT(updateNamesView()));
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::switchHighlight(bool h)
{
	if(h)
		connect(ui->L, SIGNAL(posChanged()), this, SLOT(parseAndPaint()));
	else
		disconnect(ui->L, SIGNAL(posChanged()), this, SLOT(parseAndPaint()));

}

void MainWindow::parseAndPaint()
{
    bool renderSVG = ui->S->doRender();
	hasChanged = true;
	QByteArray ba;
	devices.clear();
    devices << ui->D->pixmap;
    if (renderSVG) {
        buffer.setBuffer(&ba);
        buffer.open(QIODevice::ReadWrite);
        svg = new QSvgGenerator;
        svg->setOutputDevice(&buffer);
        svg->setViewBox(ui->D->scene->sceneRect());
        devices << svg;
    }
	foreach(QPaintDevice* pd, devices)
	{
		if(pd == svg)
			command->setSkipImages(true);
		else
		{
			command->setSkipImages(false);
		}
		command->clearTrans();
		command->clearVars();
		if(highlight->isChecked())
			command->setHighlightPP(ui->D->getHightlightPath());
		command->resetAbsolute();
		painter->begin(pd);
		if(pd == ui->D->pixmap)
		{
			painter->eraseRect(QRect(0,0,pd->width(),pd->height()));
			painter->translate(-ui->D->pixmapShift.x(), -ui->D->pixmapShift.y());
		}
		QPainterPath pp;
		command->setPP(pp);
		painter->setRenderHint(QPainter::Antialiasing);
		int cline(ui->L->currentLine());
		if(!highlight->isChecked())
			cline = -1;
		int lc(0);
		foreach(QString s, ui->L->lines())
		{
			if(!s.isEmpty())
			{
				QStringList args(s.simplified().split(" ", QString::SkipEmptyParts));
				QVariantList vl;
				foreach(const QString& a, args)
				{
					vl << a;
				}
				command->Draw(vl, lc == cline ? true : false);
			}
			++lc;
		}
		command->endDraw();
		painter->end();
	}
    if (renderSVG) {
        buffer.close();
        ui->S->updateSvg(ba);
        delete svg;
    }
    ui->D->updateScene();

}

void MainWindow::maybeSave()
{
	if(hasChanged)
	{
		QMessageBox::StandardButton qr = QMessageBox::question(this, "grrrr", "Seems that you have unsaved changes, want to save them?", QMessageBox::Ok|QMessageBox::Cancel, QMessageBox::Ok);
		if(qr == QMessageBox::Ok)
			saveTxt();
	}
}

void MainWindow::newText()
{
	maybeSave();
	command->clearAlias();
	command->clearImageCache();
	ui->L->setText(QString());
	setCurrentFile();
}

void MainWindow::loadText()
{
	maybeSave();
	QString fn(QFileDialog::getOpenFileName(this, QString("Load Text"), QDir::homePath()));
	if(fn.isEmpty())
		return;

	setCurrentFile(fn);
	QFile f(fn);
	if(f.open(QIODevice::ReadOnly))
	{
		command->clearAlias();
		command->clearImageCache();
		ui->L->blockSignals(true);
		ui->L->setText(f.readAll());
		ui->L->blockSignals(false);
		parseAndPaint();
	}
	hasChanged = false;
}

void MainWindow::saveTxt()
{
	if(currentFilename.isEmpty())
	{
		saveTxtAs();
		return;
	}
	QFile f(currentFilename);
	if(f.open(QIODevice::WriteOnly))
	{
		f.write(ui->L->text().toUtf8());
		f.close();
	}
	hasChanged = false;
}

void MainWindow::saveTxtAs()
{
	QString fn = QFileDialog::getSaveFileName ( this,  QString("Save File"), QDir::homePath());
	if(fn.isEmpty())
		return;
	setCurrentFile(fn);
	QFile f(currentFilename);
	if(f.open(QIODevice::WriteOnly))
	{
		f.write(ui->L->text().toUtf8());
		f.close();
	}
	hasChanged = false;
}

void MainWindow::saveSVG()
{
	QString	fn(QFileDialog::getSaveFileName ( this,  QString("Save SVG"), QDir::homePath()));
	if(fn.isEmpty())
		return;

	QFile f(fn);
	if(f.open(QIODevice::WriteOnly))
	{
		QByteArray ba;
		buffer.setBuffer(&ba);
		buffer.open(QIODevice::ReadWrite);
		svg = new QSvgGenerator;
		svg->setOutputDevice(&buffer);
		svg->setViewBox(ui->D->scene->sceneRect());
		painter->begin(svg);
		painter->eraseRect(QRect(0,0,svg->width(),svg->height()));
		QString text(ui->L->text());
		QStringList tl(text.split(QChar('\n'), QString::SkipEmptyParts));
		QPainterPath pp;
		command->setPP(pp);
		command->clearTrans();
		command->clearVars();
		command->resetAbsolute();
		painter->setRenderHint(QPainter::Antialiasing);
		foreach(QString s, tl)
		{
			QStringList args(s.simplified().split(" ", QString::SkipEmptyParts));
			QVariantList vl;
			foreach(const QString& a, args)
			{
				vl << a;
			}
			command->Draw(vl);

		}
		command->endDraw();
		painter->end();
		buffer.close();
		delete svg;
		f.write(ba);
		f.close();
	}
}

void MainWindow::saveImage()
{
    QString	fn(QFileDialog::getSaveFileName ( this,  QString("Save Image"), QDir::homePath()));
    if(fn.isEmpty())
        return;

    QString text(ui->L->text());
    QStringList tl(text.split(QChar('\n'), QString::SkipEmptyParts));
    QList<QVariantList> argList;
    command->clearVars();
    double pageWidth(1000.0);
    double pageHeight(1000.0);
    foreach(QString s, tl)
    {
        QStringList args(s.simplified().split(" ", QString::SkipEmptyParts));
        QString c(args.at(0));
        if (QString("page-size") == command->getFromAlias(c)) {
            pageWidth = command->number(QVariant(args.at(1)));
            pageHeight = command->number(QVariant(args.at(2)));
        }
        else {
            if (QString("var") == command->getFromAlias(c)) {
                command->insertVar(args.at(1), command->number(QVariant(args.at(2))));
            }
            QVariantList vl;
            foreach(const QString& a, args)
            {
                vl << a;
            }
            argList << vl;
        }
    }
    QPainterPath pp;
    QImage im(pageWidth, pageHeight, QImage::Format_ARGB32);
    painter->begin(&im);
    painter->eraseRect(QRect(0, 0, im.width(), im.height()));
    command->setPP(pp);
    command->clearTrans();
    command->clearVars();
    command->resetAbsolute();
    painter->setRenderHint(QPainter::Antialiasing);
    foreach(const QVariantList& vl, argList)
    {
        command->Draw(vl);
    }
    command->endDraw();
    painter->end();

    im.save(fn, "PNG");
}


void MainWindow::closeEvent(QCloseEvent * e)
{
	maybeSave();
	QMainWindow::closeEvent(e);
}


void MainWindow::setCurrentFile(const QString &fn)
{
	currentFilename = fn;
	QFileInfo info(currentFilename);
	if(fn.isEmpty())
		setWindowTitle(QString("New File - LiteralDraw"));
	else
		setWindowTitle(QString("%1 - LiteralDraw").arg(info.fileName()));
}

